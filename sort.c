#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <malloc.h> 
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h> 

struct args {
	int nproc;
	int start;
	int num;
	char** buf;
};

char** readf(char* fnin, int *icount)
{
	char** buf;
	FILE *fin;
	char ch;
	int i=0,j=0,flag=0;
	if ((fin = fopen(fnin, "r"))==NULL){
		printf("ERROR: не удается открыть исходный файл!\n");
		exit(1);
	}
	buf=(char**)malloc(sizeof(char*));
	while((ch=fgetc(fin)) != EOF){
		if (ch!='\n'){
			if (flag==1){
				j=0;
				i++;
				flag=0;		
			}
			if (j!=0){
				buf[i] = (char*) realloc(buf[i],(j + 1) * sizeof(char));
				buf[i][j]=ch;
				buf[i][j+1]='\n';
				j++;
			}
			if (j==0){
				buf=(char**)realloc(buf,(i+2)*sizeof(char*));
				buf[i] = (char*) malloc(sizeof(char));
				buf[i+1] = (char*) malloc(sizeof(char));
				buf[i][j]=ch;
				buf[i][j+1]='\n';
				buf[i+1][j]='\0';
				*icount=*icount+1;
				j++;
			}
		}
		if (ch=='\n'){
			flag=1;
		}
	}
	fclose(fin);
	return buf;
}


void *pthreadsortone(void *param)
{
	struct args pm;
	pm=*((struct args*) param);
	printf("Порожденный, строк %d,старт %d, номер %d \n", pm.num,pm.start,pm.nproc);
	char* point;
	int i=1,j=0;
	for (i=0;i<(pm.num-1);i++){
		for (j = i+1; j<(pm.num); j++){
			if (strcmp(pm.buf[i], pm.buf[j]) > 0){
				point=pm.buf[i];
				pm.buf[i]=pm.buf[j];
				pm.buf[j]=point;
			}
		}
	}
	return NULL;
}


void sort(char **buf, int *icount, char* num, int nen)
{
	int calc,calc2,calc3,inum,strctr,i1,start=0,stnum=0,nproc=0;

	inum=atoi(num);
	pthread_t thread[inum];
	struct args pm[inum];

	calc=(*icount)/(inum);
	calc2=(calc)*(inum);
	calc3=(*icount)-(calc2);
	strctr=*icount;

	for(i1=0;i1<=inum-1;i1++){
		if ((calc3>0)&&(strctr>0)){
			stnum=calc+1;
			calc3--;
		}
		else if ((calc3==0)&&(strctr>0)){
			stnum=calc;
		}
		if (stnum<=1){
			printf("Процесс№%d не запущен(сортировка из 1-й строки?)\n",i1);
		}
		else
		{
			pm[nproc].buf=buf;
			pm[nproc].start=start;
			pm[nproc].num=stnum;
			pm[nproc].nproc=nproc;
			pthread_create(&thread[nproc], NULL, pthreadsortone, &pm[nproc]);
			nproc++;
			start+=stnum;
			strctr-=stnum;
		}
	}

	for(i1=0;i1<=nproc-1;i1++) {
		pthread_join(thread[i1],NULL);
		printf("Завершение порожденного %d\n",i1);
	}
	if (nen==1){
		char* point;
		int i=1,j=0;
		for (i=0;i<(*icount-1);i++){
			for (j = i+1; j<(*icount); j++) {
				if (strcmp(buf[i], buf[j]) > 0) {
					point=buf[i];
					buf[i]=buf[j];
					buf[j]=point;
				}
			}
		}
	}
	return;
}

void writebuf(char **buf,char* path)
{
	FILE *fout;
	if ((fout = fopen(path, "w"))==NULL) {
		printf("ERROR: не удается открыть файл вывода!\n");
		exit(1);
	}
	int i=0,j=0;
	while (buf[i][j]!=NULL){
		while (buf[i][j]!='\n'){
			fputc(buf[i][j],fout);
			j++;
		}
		if (buf[i][j]=='\n'){
			fputc(buf[i][j],fout);
			free(buf[i]);
			j=0;
			i++;
		}
	}
	return;
}

void freebuf(char **buf)
{
	int i=0,j=0;
	while (buf[i][j]!=NULL)
	{
		while (buf[i][j]!='\n'){


			j++;
		}
		if (buf[i][j]=='\n'){

			free(buf[i]);
			j=0;
			i++;
		}

	}
	free(buf[i]);
	free(buf);
	return;
}

int main(int argc, char *argv[])
{
	char** buf;
	int icount=0,nen;
	if((argc!=5)&&(argc!=4)) {
		printf("ERROR: использование: %s INPUT_FILE OUTPUT_FILE, кол-во процессов, [sort(запуск общей сортировки)]) \n",argv[0]);
		exit (1);
	}
	nen=0;
	if ((argc==5)&&!strcmp(argv[4], "sort")){
		nen=1;
	}
	buf=readf(argv[1], &icount);
	sort(buf, &icount, argv[3],nen);
	writebuf(buf,argv[2]);
	freebuf(buf);
	return 0;
}
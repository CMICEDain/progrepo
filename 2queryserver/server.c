#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <malloc.h> 
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h> 
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netdb.h> 
#include <arpa/inet.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include <signal.h>
#include "sys/sem.h"

#define MQ_KEY1 1234L
#define MAXMESGDATA 50
#define N 10
#define T 8
#define L 2
#define K 2

struct stv {
	long type;
	long id;
	unsigned int timed;
	char data[MAXMESGDATA];
	int is_me;
	int num;
} mstv;
union semun {
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
                           /* часть, особенная для Linux: */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
};

void *pthreadwait(void *param)
{
	struct stv pm;
	pm=*((struct stv*) param);
	printf("Буду спать %d\n",pm.timed);
	sleep(pm.timed);

	return NULL;
}
void waitn(unsigned int timed){
	printf("Буду спать %d\n",timed);
	sleep(timed);
	return;
}

void server(int port) {
	int semid;
	pid_t mypid;
	union semun arg;
	struct msqid_ds qstatus;
	int socktcp,sockudp,newsock,newsock2,bytes_recv,cli_num,msg_id,flag=0,full;
	socklen_t clilen[2],bclilen;
	struct sockaddr_in serv_addr,bcli_addr,cli_addr[2];
	struct sembuf lock_res = {0, -1, 0};
	struct sembuf rel_res = {0, 1, 0};
	sockudp = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockudp < 0) {
		perror("ERROR opening UDP socket");
		exit(1);
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);
	serv_addr.sin_addr.s_addr = INADDR_ANY;

	printf("Сервер запущен, ожидается соединение с клиентом 1\n");

	if(bind(sockudp,(struct sockaddr *)&serv_addr, sizeof(serv_addr))<0){
		perror("ERROR on binding UDP");
		exit(1);
	}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	socktcp = socket(AF_INET, SOCK_STREAM, 0);
	if (socktcp< 0) {
		perror("ERROR opening TCP socket");
		exit(1);
	}

	if (bind(socktcp, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		perror("ERROR on binding TCP");
		exit(1);
	}	

	listen(socktcp,5);
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	clilen[0]=sizeof(cli_addr[0]);
	clilen[1]=sizeof(cli_addr[1]);

	while(1)
	{
		recvfrom(sockudp,&cli_num,sizeof(int), 0,(struct sockaddr *)&cli_addr[flag],&clilen[flag]);

		if ((cli_num==1)&&(flag==0))
		{
			printf("Сервер, соединился с клиентом 1, ожидается соединение с клиентом 2\n");
			flag=1;
		}

		if ((cli_num==2)&&(flag==1))
		{
			printf("Сервер, соединился с клиентом 2\n");
			break;
		}
	}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	cli_num=0;
	
	if((msg_id=msgget(ftok("/home/dain/learning/1234", MQ_KEY1), 0666|IPC_CREAT))==-1){
		perror("msgget failed");
		exit(1);
	}

	if(msgctl(msg_id,IPC_STAT,&qstatus)<0){
		perror("msgctl failed");
		exit(1);
	}
	semid = semget(ftok("/home/dain/learning/1234", MQ_KEY1), 1, 0666 | IPC_CREAT);
	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);
	mypid=fork();
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	if (mypid!=0){

		//получение нужных сокетов**************************************
		bclilen=sizeof(bcli_addr);
		while(1){

			printf("Пытаемся получить сокеты\n");

			newsock = accept(socktcp,(struct sockaddr *) &bcli_addr, &bclilen);
			if (newsock < 0) {
				perror("ERROR on accept 1");
				exit(1);
			}

			bytes_recv = read(newsock,&cli_num,sizeof(int));
			if (bytes_recv < 0) {
				perror("ERROR reading from socket");
				exit(1);
			}

			if(cli_num==235){
				printf("Сервер, сокет клиента №1 получен\n");
				break;
			}


		}

		while(1){

			newsock2 = accept(socktcp,(struct sockaddr *) &bcli_addr, &bclilen);
			if (newsock2 < 0) {
				perror("ERROR on accept 2");
				exit(1);
			}

			bytes_recv = read(newsock2,&cli_num,sizeof(int));
			if (bytes_recv < 0) {
				perror("ERROR reading from socket");
				exit(1);
			}

			if(cli_num==2389){
				printf("Сервер, сокет клиента №2 получен\n");
				break;
			}
		}
		//конец, нужные сокеты получены*************************************
		char wmsg[5]="full";
		full=0;
		while(1){

			if(msgctl(msg_id,IPC_STAT,&qstatus)<0){
				perror("msgctl failed");
				exit(1);
			}
			if((qstatus.msg_qnum==N)&&(full==0)){
				if (semop(semid, &lock_res, 1) == -1){
					perror("semop:lock_res");
				}
				full=1;
			}
			if((qstatus.msg_qnum==0)&&(full==1)){
				semop(semid, &rel_res, 1);
				full=0;
			}
			if(full==0){

				bytes_recv = read(newsock,&mstv,sizeof(mstv));

				if (bytes_recv < 0) {
					perror("ERROR reading from socket");
					exit(1);
				}
				if(msgsnd(msg_id,&mstv,sizeof(mstv),IPC_NOWAIT) < 0) {
					perror("msgsnd failed");
					exit(1);
				}
				printf("Записано\n");
			}

			if(full==1){
				
				if(sendto(sockudp,&wmsg, sizeof(wmsg), 0, (struct sockaddr *)&cli_addr[1],sizeof(cli_addr[1]))<0){
					perror("ERROR sending UDP to second client");
					exit(1);
				}
				
				if((msgrcv(msg_id,&mstv,sizeof(mstv), mstv.id,0)) < 0) {
					perror("msgrcv failed");
					exit(1);
				}

				printf("Структур %d, отправляем %d\n",qstatus.msg_qnum,mstv.num);
				send(newsock2, &mstv, sizeof(mstv), 0);
				sleep(L);
			}

		}
	}
	
	if (mypid==0){
		
		char wmsg[7]="waiting";
		while(1){
			
			sleep(K);
			if (semop(semid, &lock_res, 1) == -1){
				perror("semop:lock_res");
			}
			printf("Запрос структуры\n");

			if(sendto(sockudp,&wmsg, sizeof(wmsg), 0, (struct sockaddr *)&cli_addr[0],sizeof(cli_addr[0]))<0){
				perror("ERROR sending UDP");
				exit(1);
			}
			semop(semid, &rel_res, 1);


		}
	}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		msgctl(msg_id,IPC_RMID,NULL);
		return;

	}


	void client1(char* IP,int port) 
	{

		int sockudp,socktcp,i;
		struct sockaddr_in serv_addr;
		char wmsg[7];
		int j=1;

		sockudp = socket(AF_INET, SOCK_DGRAM, 0);
		if (sockudp < 0) {
			perror("ERROR opening UDP socket");
			exit(1);
		}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(port);
		serv_addr.sin_addr.s_addr =inet_addr(IP);

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		if(sendto(sockudp,&j, sizeof(int), 0, (struct sockaddr *)&serv_addr,sizeof(serv_addr))<0){
			perror("ERROR sending UDP");
			exit(1);
		}

		socktcp = socket(AF_INET, SOCK_STREAM, 0);
		if (socktcp < 0) 
			perror("ERROR opening TCP socket");


		if (connect(socktcp,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
			perror("ERROR connecting");

		i=235;
		send(socktcp, &i, sizeof(int), 0);
		printf("Sended\n");
		mstv.is_me=1;
		mstv.timed=T;
		mstv.id=1;
		mstv.type=1;
		mstv.num=1;
		strcpy( mstv.data, "jftrd" );
		i=1;
		while(1){
			recvfrom(sockudp,&wmsg,sizeof(wmsg), 0,NULL,NULL);
			printf("Клиент 1, отправка структуры № %d\n",i);
			i++;
			send(socktcp, &mstv, sizeof(mstv), 0);
			mstv.num=i;
		}
		return;
	}

	void client2(char* IP,int port) 
	{
		int sockudp,socktcp,i,bytes_recv;
		struct sockaddr_in serv_addr;
		char wmsg[7];
		int j=2;
		sockudp = socket(AF_INET, SOCK_DGRAM, 0);
		if (sockudp < 0) {
			perror("ERROR opening UDP socket");
			exit(1);
		}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(port);
		serv_addr.sin_addr.s_addr =inet_addr(IP);

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		if(sendto(sockudp,&j, sizeof(int), 0, (struct sockaddr *)&serv_addr,sizeof(serv_addr))<0){
			perror("ERROR sending UDP");
			exit(1);
		}

		socktcp = socket(AF_INET, SOCK_STREAM, 0);
		if (socktcp < 0) 
			perror("ERROR opening TCP socket");


		if (connect(socktcp,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
			perror("ERROR connecting");

		i=2389;
		send(socktcp, &i, sizeof(int), 0);
		printf("Sended\n");

		while(1){

			recvfrom(sockudp,&wmsg,sizeof(wmsg), 0,NULL,NULL);

			bytes_recv = read(socktcp,&mstv,sizeof(mstv));
			if (bytes_recv < 0) {
				perror("ERROR reading from socket");
				exit(1);
			}

			printf("Клиент 2, принял структуру %d\n",mstv.num);
			printf("Буду спать %d\n",mstv.timed);
			sleep(mstv.timed);

		}

		return;
	}


	void clr()
	{int msg_id;

		if((msg_id=msgget(ftok("/home/dain/learning/1234", MQ_KEY1), 0666|IPC_CREAT))==-1){
			perror("msgget failed");
			exit(1);
		}

		msgctl(msg_id,IPC_RMID,NULL);

		return;
	}


	int main(int argc, char *argv[])
	{
		if ((argc<2)||(argc>4)){
			printf("Неверно задан аргумент, использование: %s (server port)|(client[1|2], IP addres, port)|(queueclr)\n", argv[0]);
			exit(0);
		};

		if ((argc==2)&&!strcmp(argv[1], "queueclr"))
		{
			clr();
		}

		if ((argc==4)&&!strcmp(argv[1], "client1"))
		{
			int sockport=atoi(argv[3]);
			client1 (argv[2],sockport);
		}

		if ((argc==4)&&!strcmp(argv[1], "client2"))
		{
			int sockport=atoi(argv[3]);
			client2 (argv[2],sockport);
		}

		if ((argc==3)&&!strcmp(argv[1], "server"))
		{
			int sockport=atoi(argv[2]);
			server (sockport);
		}

		return 0;
	} 